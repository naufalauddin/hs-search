{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
module HSearch.CompressionSpec where

import HSearch.Compression
import Test.Hspec

spec :: Spec
spec = do
	describe "variable byte encoding" do
		describe "encode" do
			it "0" do
				vbe_encode 0 `shouldBe` "\x00"
			it "127" do
				vbe_encode 127 `shouldBe` "\x7F"
			it "128" do
				vbe_encode 128 `shouldBe` "\x81\x00"
		describe "decode" do
			context "correct encoding" do 
				it "0" do
					vbe_decode "\NUL" `shouldBe` Right 0
				it "127" do
					vbe_decode "\x7F" `shouldBe` Right 127
				it "128" do
					vbe_decode "\x81\x00" `shouldBe` Right 128
			context "error" do
				it "empty" do
					vbe_decode "" `shouldBe` Left EmptyByteString
				it "end with length bit set" do
					vbe_decode "\x80" `shouldBe` Left (EndWithPartial 0)
				it "end with length bit set and carry partial information" do
					vbe_decode "\x84" `shouldBe` Left (EndWithPartial 4)