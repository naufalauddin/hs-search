module Main (main) where

import Test.Hspec.Runner
import qualified Spec

main :: IO ()
main = do
    -- hspecWith (useFormatter ("checks", checks) defaultConfig) Spec.spec
    hspec Spec.spec