{-# LANGUAGE OverloadedStrings #-}
module HSearch.Compression (vbe_encode, vbe_decode, ParseError(..)) where

import Control.Exception
import qualified Data.ByteString.Lazy as BL
import Data.ByteString.Builder
import Data.Char
import Data.Bits

vbe_encode :: Word -> BL.ByteString
vbe_encode = toLazyByteString . encode
    where
        encode n
            | n < 128 = char7 $ chr (fromIntegral n)
            | otherwise = encodeCont (n .>>. 7) <> encode (n .&. 0x7F)
        encodeCont n
            | n < 128 = char8 $ chr (fromIntegral n `setBit` 7)
            | otherwise = encodeCont (n .>>. 7) <> encodeCont (n .&. 0x7F)

data ParseError
    = EmptyByteString
    | EndWithPartial Word
    deriving (Show, Eq)

instance Exception ParseError

vbe_decode :: BL.ByteString -> Either ParseError Word
vbe_decode b = decode b 0
    where
        decode bytes acc =
            case BL.uncons bytes of
                Nothing -> Left EmptyByteString
                Just (n, "") | n >= 128 -> Left $ EndWithPartial $ (acc .<<. 7) .|. (fromIntegral (n .&. 0x7f))
                Just (n, rest) ->
                    if n < 128
                        then Right $ (acc .<<. 7) .|. (fromIntegral (n .&. 0x7f))
                        else decode rest ((acc .<<. 7) .|. (fromIntegral (n .&. 0x7f)))