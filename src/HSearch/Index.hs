module HSearch.Index where

import Data.IntMap (IntMap)
import qualified Data.IntMap.Strict as IM
import Data.Text (Text)
import qualified Data.Text as Text

type IdDocMap = IntMap Text